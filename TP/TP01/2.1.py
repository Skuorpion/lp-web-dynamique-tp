mass=42.0
chars = {
    'earth': {'name': 'earth', 'G': 1 ,},
    'mercury': {'name': 'mercury', 'G': 0.378,},
    'moon': {'name': 'moon', 'G': 0.166,},
    'jupiter': {'name': 'jupiter', 'G': 2.364,},
}

for name in chars:
    #print('The mass of the object on %(who)s is %(mass)f.' % {'who': chars[name]['name']} % {'mass': chars[name]['G']})
    #print('The mass of the object on', chars[name]['name'], 'is', chars[name]['G']*mass, '.')
    print('The mass of the object on {0} is {1}.' .format( chars[name]['name'], chars[name]['G']*mass))