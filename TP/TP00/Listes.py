
ints = [0, 1, 1, 2, 2, 3, 5, 8, 13]
print(ints)
print(type(ints))
print(ints[6])

ints[6] = 'Hello'
print(ints)
ints.pop(4)
print(ints)

ints.append(42)
print(ints)
other = ['etc.']
print(ints + other)
print(ints[2:])
len(ints)

"""

print([x ** 2 for x in range(6)])
"""