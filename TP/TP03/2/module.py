# coding: utf8

def say_hello(name):
    return 'Hello %s !' % name

if __name__ == '__main__':
    print (say_hello('Ben Kenobi'))