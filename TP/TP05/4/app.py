from flask import Flask
app = Flask(__name__)

#@app.route('/')
#def index():
#    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello, World'

@app.route('/say_hello/<name>')
def say_hello(name):
    return "Hello %s" % name

@app.route('/multiply/<int:facteur_gauche>/<int:facteur_droite>')
def multiply(facteur_gauche, facteur_droite):
    return "%s" %(facteur_gauche * facteur_droite)

@app.route('/index')
def index():
    return 'Jurassic Park index'


#@app.route('/<int:facteur_gauche>/<op>/<int:facteur_droite>')
#def calcul(facteur_gauche, op, facteur_droite):
#    return "%s" %(facteur_gauche op facteur_droite)